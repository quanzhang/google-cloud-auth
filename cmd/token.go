/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"context"
	"fmt"
	"gitlab/quanzhang/google-cloud-auth/pkg/auth"

	"github.com/spf13/cobra"
)

// tokenCmd represents the access-token command
var tokenCmd = &cobra.Command{
	Use:   "access-token",
	Short: "Print the access token from credential file from GOOGLE_APPLICATION_CREDENTIALS",
	Long: `Print an access token for the specified account. See RFC6749
	(https://tools.ietf.org/html/rfc6749) for more information about access
	tokens.
	Note that token itself may not be enough to access some services. If you
    use the token with curl or similar tools, you may see permission errors
    similar to "API has not been used in project 32555940559 before or it is
    disabled.". If it happens, you may need to provide a quota project in the
    "X-Goog-User-Project" header. For example,

        $ curl -H "X-Goog-User-Project: your-project" \
            -H "Authorization: Bearer $(gcloud auth print-access-token)" \
            foo.googleapis.com

    The identity that granted the token must have the serviceusage.services.use
    permission on the provided project. See
    https://cloud.google.com/apis/docs/system-parameters for more information.
	`,
	RunE: func(cmd *cobra.Command, args []string) error {
		token, err := auth.AccessToken(context.Background())
		if err != nil {
			return err
		}
		fmt.Println(token)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(tokenCmd)
}

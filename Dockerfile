# Use a specific version of the Golang image as the builder stage
FROM golang:1.19 as builder
WORKDIR /app
COPY . .
RUN go build -o google-cloud-auth

# Start a new stage using a Debian-based image
FROM ubuntu:latest
WORKDIR /app
RUN apt-get update && apt-get install -y --no-install-recommends \
    bash \
    curl \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /app/google-cloud-auth /usr/bin/google-cloud-auth
CMD ["google-cloud-auth"]
